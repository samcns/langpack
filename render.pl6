#!/usr/bin/env perl6

use v6;
use YAMLish;

my $svg  = slurp 'originals'.IO.child('0001_are_you_from_space.svg');

for ['en', 'fr', 'ja'] -> $lang {
    my %i18n = load-yaml slurp 'i18n'.IO.child("0001.{$lang}.yml");
    my $out  = $svg;

    map -> $t {
        $out = $out.subst: $t.key, $t.value, :g;
    }, %i18n;

    spurt 'renders'.IO.child($lang).child('0001_are_you_from_space.svg'), $out; 
}
